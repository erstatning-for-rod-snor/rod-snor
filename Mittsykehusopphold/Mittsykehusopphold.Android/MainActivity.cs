﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Microsoft.WindowsAzure.MobileServices;

namespace Mittsykehusopphold.Droid
{


    [Activity(Label = "Mittsykehusopphold", Icon = "@drawable/login", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);


            Microsoft.WindowsAzure.MobileServices.MobileServiceClient MittsykehusoppholdClient = new Microsoft.WindowsAzure.MobileServices.MobileServiceClient(
            "https://mittsykehusopphold.azurewebsites.net");
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
           
            LoadApplication(new App());

            CurrentPlatform.Init();
            
        }
    }
}

