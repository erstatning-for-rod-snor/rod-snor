﻿using M2Mqtt;
using M2Mqtt.Messages;
using Microsoft.WindowsAzure.MobileServices;
using Mittsykehusopphold.Message;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mittsykehusopphold
{
    public class MyMobileClient
    {
        MobileServiceClient _mobileService;

        public int RegisterTable { get; internal set; }

        public MyMobileClient()
        {
        }

        public MyMobileClient(MobileServiceClient mobileService)
        {
            _mobileService = mobileService;
        }

        public async void StoreData(HelpMessage data)
        {
            TodoItem item = new TodoItem { Text = data.Message, Location = data.Location };
            await _mobileService.GetTable<TodoItem>().InsertAsync(item);
        }

        ////lager funskjon for å hente data fra sql/ microsoft azure
        public IEnumerable<TodoItem> fetchPendingTasksFromCloud()
        {
            var db = _mobileService.GetTable<TodoItem>().ReadAsync("select * from TodoItem");
            var test = db.Result;
            return new List<TodoItem>();

            // https://docs.microsoft.com/en-us/azure/app-service-mobile/app-service-mobile-dotnet-how-to-use-client-library#querying
            //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/async/
        }
    }
}
