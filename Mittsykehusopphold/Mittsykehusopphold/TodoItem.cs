﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace Mittsykehusopphold
{
    public class TodoItem
    {
        //En liste som blir lagret i Databasen. så velger jeg ut det jeg vil hente. 

        public string id { get; set; }

        [JsonProperty(PropertyName = "createdat")]
        public string createdAt { get; set; }

        [JsonProperty(PropertyName = "updatedat")]
        public string updatedAt { get; set; }

        [JsonProperty(PropertyName = "version")]
        public string version { get; set; }

        [JsonProperty(PropertyName = "deleted")]
        public bool deleted { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "complete")]
        public bool Complete { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }
      
    }

}
