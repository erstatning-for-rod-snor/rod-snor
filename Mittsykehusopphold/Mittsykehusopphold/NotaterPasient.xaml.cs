﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotaterPasient : ContentPage, INotifyPropertyChanged
    {

        public NotaterPasient()
        {
            InitializeComponent();
        }

        public class Person
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public bool IsVisible { get; set; }

            public Person()
            {
                this.Code = DateTime.Now.ToString("hhmmssffffff");
                this.Name = "Mr. A";
                this.Address = "Singapore";
                this.IsVisible = true;
            }

        }

        public class PeoplePair : Tuple<Person, Person>
        {
            public PeoplePair(Person item1, Person item2)
                : base(item1, item2 ?? CreateEmptyModel()) { }

            private static Person CreateEmptyModel()
            {
                return new Person { IsVisible = false };
            }
        }

        public class PersonViewModel : INotifyPropertyChanged
        {
            public PersonViewModel()
            {
                //Create mock data
                People = new List<PeoplePair>
            {
                new PeoplePair(new Person(), new Person()),
                new PeoplePair(new Person(), new Person()),
                new PeoplePair(new Person(), new Person()),
                new PeoplePair(new Person(), null)
            };
                //Add an empty data
            }

            public List<PeoplePair> People { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }

        }
    }
}

//http://blog.zquanghoangz.com/?p=105

    