﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class login : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public login()
        {

        }

        public login (MyMobileClient myMobileClient)
		{
			InitializeComponent ();
            _myMobileClient = myMobileClient;
        }

        public login(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void Button_Clicked (object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button");
            NextPage();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new Helsepersonell(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }
    }
}