﻿using M2Mqtt;
using M2Mqtt.Messages;
using Microsoft.WindowsAzure.MobileServices;
using Mittsykehusopphold.Message;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mittsykehusopphold
{
  
     public class MyMqtt : INotifyPropertyChanged
    {
        private MqttClient _client;
        private string _topic;
        //Lager en clientId her som kun lages en gang
        private string _clientId;
        private string _message;
        private HelpMessage _helpMessage;

        //conneter meg til databasen og gjør denne funksjon global
        private Context db = new Context("bachlor-ele301.database.windows.net", "Fawad", "123456Rev", "bachlor-ele301");

        private Message.Decode myMessage = new Message.Decode();

        public event PropertyChangedEventHandler PropertyChanged;

        internal void Subscribe(string[] v1, byte[] v2)
        {
            throw new NotImplementedException();
        }

        public MyMqtt()
        {

        }

        public MyMqtt(string address, int port)
        {
            _client = new MqttClient(address, port, false, MqttSslProtocols.None, null, null);
            HelpMessage = new HelpMessage();
        }


        public void Connect(string user, string password)    //setter ClientId hver gang og bruker den internt i klassen.
        {
            _client.Connect(_clientId, "ybgfmoyh", "Gvg1tOesRDaW");  // setter client id, bruker og passord
        }

        public void SendMessage(string message)                     
        {
            //sender en melding til Topic
            _client.Publish(_topic, Encoding.ASCII.GetBytes(message));
        }

        public void SubscribeTopic()                                  
        {
            //henter data fra Topic i clouden
            _client.Subscribe(new string[] { _topic }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            _client.MqttMsgPublishReceived += _client_MqttMsgPublishReceived;
        }

        public void UnSubsribeTopic()
        {
            _client.MqttMsgPublishReceived -= _client_MqttMsgPublishReceived;
        }

        private void _client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            Message = string.Empty;
            HelpMessage.Clear();

            Message = Encoding.UTF8.GetString(e.Message);
            //eksempel Message fra Rasberry pi = "M1_C6:2B:45:5F:71:FD2";
            if (Message.Contains('_'))
            { 
                HelpMessage.Bluetooth = true;
            }

            HelpMessage = myMessage.DecodeMessage(Message);
            var item = new TodoItem
            {
                Text = HelpMessage.Message,
                Complete = false,
                Location = HelpMessage.Location
            };


            //Kun brukes hvis EN Mobil brukes. Blåtann data må lagres fra raspberry. App data fra pasient må lagres fra mobil
            //Fungerer hvis med 2 mobiler hvis en er pasient og en er helsepersonel
            db.Save(item);

            NotifyPropertyChanged("Message");
            NotifyPropertyChanged("HelpMessage");

        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Topic
        {
            get
            {
                return _topic;
            }
            set
            {
                _topic = value;
            }
        }

        public string ClientId
        {
            get
            {
                return _clientId;
            }
            set
            {
                _clientId = value;
            }
        }

        public string Message
        {
            get
            {

                return _message;
            }

            set
            {
                _message = value;
                //NotifyPropertyChanged();
            }
        }

        public HelpMessage HelpMessage
        {
            get
            {
                return _helpMessage;
            }
            set
            {
                _helpMessage = value;
                //NotifyPropertyChanged();
            }
        }
    }
}
