﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Helsepersonell : ContentPage
    {
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Helsepersonell()
        {

        }

        public Helsepersonell(MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public Helsepersonell(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            //Dokumentasjonplik knapp. 
        }
        private void Butten_2_Clicked(object sender, EventArgs e)
        {
            //Notater Pasient knapp
            NextPage2();
        }
        private void Butten_3_Clicked(object sender, EventArgs e)
        {
            //Knapp til status
            NextPage3();
        }

        private async void NextPage3()
        {
            //Her tar vi med oss  _myMobileClient og _myMqtt
            var newPage = new NavigationPage(new Varsel(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }

        private async void NextPage2()
        {
            var newPage = new NavigationPage(new NotaterPasient());
            await Navigation.PushModalAsync(newPage);
        }
    }
}