﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Helsepersonllet : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Helsepersonllet ()
		{
			
		}

        public Helsepersonllet(MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public Helsepersonllet(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private  async void ButtonHelp_Clicked(object sender, EventArgs e)
        {
           
            _myMqtt.Topic = "helse/varsel";

            //Melding blir sendt til cloud
            _myMqtt.SendMessage("M1@Pasient:" + customText.Text + "@" + _myMqtt.HelpMessage.Location);

            //Melding blir sendt til SQL database
            _myMqtt.HelpMessage.Message = "kontankter helsepersnonellet";
            
            
            await DisplayAlert("Selected", "Du ønsker å få hjelp av en Helsepersonell. Kontakter Helsepersonelle", "OK");
        }
    }
}