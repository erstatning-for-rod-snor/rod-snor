﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Hjelp : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Hjelp()
        {

        }

        public Hjelp (MyMobileClient myMobileClient)
		{
			InitializeComponent ();
            _myMobileClient = myMobileClient;
        }

        public Hjelp(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            NextPage();
        }
        private async void Butten_WC_Clicked(object sender, EventArgs e)
        {
            _myMqtt.Topic = "helse/varsel";

            //melding sendt til Cloud
            _myMqtt.SendMessage("M1@Pasient:Toalett/WC@" + _myMqtt.HelpMessage.Location);

            //melding sendt til SQL database
            _myMqtt.HelpMessage.Message = "Pasient:Toalett/WC"; /*Pasient ønsker å gå på WC*/

            //_myMobileClient.StoreData(_myMqtt.HelpMessage); en annen metode til å sende varslet til Sql data base

            await DisplayAlert("WC", "Du ønsker å få hjelp til å gå på WC. Kontakter Helsepersonelle", "Ok");
        }
        
        private void Butten_3_Clicked(object sender, EventArgs e)
        {
            NextPage1();
        }
        private async void Butten_akutt_Clicked(object sender, EventArgs e)
        {
            string action = await DisplayActionSheet("Akutthjelp", "Avbryt", null, "Ring Brannvesenet", "Ring politi", "Ring medisinsk nødhjelp");
        }

        private void Button_4_Clicked(object sender, EventArgs e)
        {
            NextPage4();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new Helsepersonllet(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage); 
        }
        private async void NextPage1()
        {
            var newPage = new NavigationPage(new UkeMat(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }

        private async void NextPage4()
        {
            var newPage = new NavigationPage(new Pasient(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }
    }
}