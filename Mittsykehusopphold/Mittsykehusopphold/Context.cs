﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
using Xamarin.Forms;

namespace Mittsykehusopphold
{
    public class Context : IDisposable
    {
        private string _dataSource;
        private string _user;
        private string _password;
        private string _initialCatalog;
        private SqlConnectionStringBuilder _builder;
        private SqlConnection _connection;

        public Context(string dataSource, string user, string password, string initialCatalog)
        {
            _builder = new SqlConnectionStringBuilder() { DataSource = dataSource, UserID = user, Password = password, InitialCatalog = initialCatalog};
        }


        private void Connect()
        {
            Connection = new SqlConnection(_builder.ConnectionString);

            Connection.Open();
        }

        public ObservableCollection<TodoItem> GetData(string query)
        {
            ObservableCollection<TodoItem> rows = new ObservableCollection<TodoItem>();

            try
            {
                Connect();

                using (SqlCommand command = new SqlCommand(query, Connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TodoItem row = new TodoItem();
                            row.id = reader["id"].ToString();
                            row.createdAt = reader["createdAt"].ToString();
                            row.updatedAt = reader["updatedAt"].ToString();
                            row.version = reader["version"].ToString();
                            row.deleted = Convert.ToBoolean(reader["deleted"]);
                            row.Text = reader["Text"].ToString();
                            row.Complete = Convert.ToBoolean(reader["Complete"]);
                            row.Location = reader["Location"].ToString();

                            rows.Add(row);
                        }
                    }
                }

                Connection.Close();
            }
            catch(Exception ex)
            {
                Connection.Close();
            }

            return rows;     
        }

         //oppdaterer data i Sql og applikasjonen 
        public void UpdateData(string query)
        {
            try
            {
                Connect();

                using (SqlCommand command = new SqlCommand(query, Connection))
                {
                    command.ExecuteNonQuery();
                }

                Connection.Close();
            }
            catch (Exception ex)
            {
                Connection.Close();
            }
        }

        //lager en funkjson slikt at jeg lagrer vaslene i sql database
        public void Save(TodoItem item)
        {
            try
            {
                Connect();


                using (SqlCommand command = new SqlCommand("Insert into todoitem (Text, Complete, Location) Values (@text, @complete, @location);", Connection))
                {
                    command.Parameters.AddWithValue("@text",item.Text);
                    command.Parameters.AddWithValue("@complete", 0);
                    command.Parameters.AddWithValue("@location", item.Location);
                    command.ExecuteNonQuery();
                }

                Connection.Close();
            }
            catch (Exception ex)
            {
                Connection.Close();
            }
        }

        public void Dispose()
        {
            
        }

        public SqlConnection Connection { get => _connection; set => _connection = value; }
    }
}
