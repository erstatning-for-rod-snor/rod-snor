﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registering : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Registering ()
		{
			
		}

        public Registering(MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public Registering(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button");
            DisplayActionSheet("Registering", "JA", "Nei", "Navn:" + NaText.Text  , "Rom:" + RomText.Text , "Området:" + omText.Text, "Stemmer dette?");
            NextPage();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new loginpasient(_myMobileClient));
            await Navigation.PushModalAsync(newPage);
        }
    }
}