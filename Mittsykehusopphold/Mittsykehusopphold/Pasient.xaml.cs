﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Pasient : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Pasient ()
        {

        }

        public Pasient (MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public Pasient(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
            
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            NextPage();
        }

        private void Butten_2_Clicked(object sender, EventArgs e)
        {         
            NextPage2();
        }

        private void Butten_3_Clicked(object sender, EventArgs e)
        {          
            //("Pasient trykket Pulsambånd");
        }

        private void Butten_4_Clicked(object sender, EventArgs e)
        {
            //("Pasient trykket HJELP");
            NextPage4();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new Mingjoremol());
            await Navigation.PushModalAsync(newPage);
        }

        private async void NextPage2()
        {
            var newPage = new NavigationPage(new Medisin(_myMobileClient));
            await Navigation.PushModalAsync(newPage);
        }

        private async void NextPage4()
        {
            var newPage = new NavigationPage(new Hjelp(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }
    }
}