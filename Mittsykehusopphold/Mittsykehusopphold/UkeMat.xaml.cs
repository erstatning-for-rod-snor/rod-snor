﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UkeMat : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public UkeMat()
        {

        }

        public UkeMat (MyMobileClient myMobileClient)
		{
			InitializeComponent ();
            _myMobileClient = myMobileClient;
        }

        public UkeMat(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void ImageCell_BindingContextChanged(object sender, EventArgs e)
        {

        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button");
            NextPage();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new Mat(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        } 
    }
}