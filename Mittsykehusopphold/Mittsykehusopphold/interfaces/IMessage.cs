﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mittsykehusopphold.interfaces
{
    public interface IMessage
    {
        //enums.enums.VarselEnum VarselEnum { get; set; }

        T Make<T>(string message);
    }
}
