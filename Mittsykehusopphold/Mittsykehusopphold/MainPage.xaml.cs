﻿using M2Mqtt;
using M2Mqtt.Messages;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mittsykehusopphold
{
    public partial class MainPage : ContentPage
    {
        MobileServiceClient _mobileService;
        MyMobileClient _myMobileClient;
        MyMqtt _myMqttClient;

        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            //microsoft Azure
            _mobileService = new MobileServiceClient("https://mittsykehusopphold.azurewebsites.net");
            _myMobileClient = new MyMobileClient(_mobileService);
            //CloudMqtqqt
            //Vi lager en client Id her som vi lagrer i myMqtt objectet
            _myMqttClient = new MyMqtt("m24.cloudmqtt.com", 15897);
            _myMqttClient.ClientId = Guid.NewGuid().ToString();
            _myMqttClient.Connect("ybgfmoyh", "Gvg1tOesRDaW"); 
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button_2");
            NextPage();
        }

        private void Butten_2_Clicked(object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button_2");
            NextPage2();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new loginpasient(_myMobileClient, _myMqttClient));
            await Navigation.PushModalAsync(newPage);
        }

        private async void NextPage2()
        {
            var newPage = new NavigationPage(new login(_myMobileClient, _myMqttClient));
            await Navigation.PushModalAsync(newPage);
        }
    }
}
