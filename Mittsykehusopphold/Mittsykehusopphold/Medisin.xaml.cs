﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Medisin : ContentPage
	{
        private MyMobileClient _myMobileClient;

        public Medisin()
        {

        }

        public Medisin (MyMobileClient myMobileClient)
		{
			InitializeComponent ();
            _myMobileClient = myMobileClient;
        }
	}
}