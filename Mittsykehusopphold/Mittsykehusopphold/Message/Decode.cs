﻿using Mittsykehusopphold.interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mittsykehusopphold.Message
{
    public class Decode
    {
        private string _message;

        public Decode()
        {
            
        }

        public Decode(string message)
        {
            _message = message;
        }

        public HelpMessage DecodeMessage(string message)
        {              
           //definerer hvor meldingen kommer fra de ulike enheten
            string[] type = new string[2];
                  
            if(message.Contains("_"))
            {
                type = message.Split('_');
            }
            else if (message.Contains("@"))
            {
                type = message.Split('@');
            }

            var helpMessage = new HelpMessage();
            
                   //definerer de ulike knappene fra Fysiske enheten
            switch (type[0])
            {
                case "M1":
                    helpMessage.Message = "Akutt hjelp";
                    break;
                case "M2":
                    helpMessage.Message = "Medisin";
                    break;
                case "M3":
                    helpMessage.Message = "Toalett/WC";
                    break;
                case "M4":
                    helpMessage.Message = "Annet";
                    break;
                default:

                    break;
            }

            //Hvis trykknapp sjekk på mac adresse
            if (type.Length == 2)
            {
                if (type[1] == "C6:2B:45:5F:71:FD")
                {
                    helpMessage.Location = "Rom 5";
                }
                else
                {
                    //default verdi
                    helpMessage.Location = "Fellesrom";
                }
            }
            if (type.Length == 3)
            {
                helpMessage.Message = type[1];
                helpMessage.Location = type[2];
            }

            //Lokasjon fra app ligger inne i programmet når pasient logger på
            //else
            //{
            //    helpMessage.Location = "Rom *";
            //}


            return helpMessage;
        }
    }
}
