﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mittsykehusopphold.Message
{
    public class HelpMessage
    {
       //definerer meldingtjenetse og Location som vil være rom
        public string Message { get; set; }
        public string Location { get; set; }
        public bool Bluetooth { get; set; } = false;

        public void Clear()
        {
            Message = string.Empty;
            Location = string.Empty;
            Bluetooth = false;
        }
    }
}
