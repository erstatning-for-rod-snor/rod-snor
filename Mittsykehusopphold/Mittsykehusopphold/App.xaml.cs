﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;



[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Mittsykehusopphold
{
     

    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
           

            //mainpage til å starte applikasjonen med
            MainPage = new MainPage();

        }

        protected override void OnStart()
        {
            // Handle when your app starts
              
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            
        }
    }
}
