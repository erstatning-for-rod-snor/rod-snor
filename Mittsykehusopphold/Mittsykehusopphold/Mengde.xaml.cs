﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Mengde : ContentPage
    {
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Mengde()
        {
        }

        public Mengde(MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public Mengde(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            _myMqtt.Topic = "helse/varsel";

            //sender medling til Cloud med tekst og romnummer
            _myMqtt.SendMessage("M5@Pasient bestilte hot tomatsuppe:" +"Porsjon:" + PorsjonText.Text + "Kommentar:" + KommentarText.Text + "@" + _myMqtt.HelpMessage.Location);

            //lagrer varslet til database. 
            _myMqtt.HelpMessage.Message = "Bestilte: Hot Tomatsuppe"; /*Pasient bestilte Hot Tomatsuppe*/

            //_myMobileClient.StoreData(_myMqtt.HelpMessage);

            //pop-ups display som viser hva pasienten har bestilt. 
            await DisplayActionSheet("Hot Tomatsuppe", "OK", "", "Porsjon:" + PorsjonText.Text +"suppe", "Kommentar:" + KommentarText.Text, "Kontakter Helsepersonel");
        }
    }
}