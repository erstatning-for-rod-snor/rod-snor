﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Mat : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public Mat()
        {

        }

        public Mat (MyMobileClient myMobileClient)
		{
			InitializeComponent ();
            _myMobileClient = myMobileClient;
        }

        public Mat(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void ImageCell_Tapped(object sender, EventArgs e)
        {
            var test = this.FindByName<ImageCell>("tapped");
            NextPage();
        }

        private async void NextPage()
        {
            var newPage = new NavigationPage(new Mengde(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }
    }
}