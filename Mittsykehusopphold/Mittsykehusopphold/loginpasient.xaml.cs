﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mittsykehusopphold
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class loginpasient : ContentPage
	{
        private MyMobileClient _myMobileClient;
        private MyMqtt _myMqtt;

        public loginpasient ()
		{
			
		}

        public loginpasient(MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public loginpasient(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button");
            NextPage();
        }

        private void Button_Clicked2(object sender, EventArgs e)
        {
            var test = this.FindByName<Button>("button");
            NextPage2();
        }

        private async void NextPage()
        {
            _myMqtt.HelpMessage.Location = this.RoomNumber.Text;
            var newPage = new NavigationPage(new Hjelp(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }

        private async void NextPage2()
        {
            var newPage = new NavigationPage(new Registering(_myMobileClient, _myMqtt));
            await Navigation.PushModalAsync(newPage);
        }
    }
}