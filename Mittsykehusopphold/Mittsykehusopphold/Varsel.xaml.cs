﻿
using M2Mqtt;
using M2Mqtt.Messages;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;


namespace Mittsykehusopphold
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Varsel : ContentPage, INotifyPropertyChanged
    {
        private MyMobileClient _myMobileClient;
        private static MyMqtt _myMqtt;
        private static string _topic;
        private Message.Decode myMessage = new Message.Decode();
        private object lockObject = new object();
        private ObservableCollection<TodoItem> _todoitemlist;
        
        public event PropertyChangedEventHandler TodoListChanged;

        public Varsel()
        {

        }

        public Varsel(MyMobileClient myMobileClient)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
        }

        public Varsel(MyMobileClient myMobileClient, MyMqtt myMqtt)
        {
            InitializeComponent();
            _myMobileClient = myMobileClient;
            _myMqtt = myMqtt;
            _myMqtt.Topic = "helse/varsel"; //f.eks.
            SubscribeTopic();
            _myMqtt.PropertyChanged += _myMqtt_PropertyChanged;
            sqlazure();         
        }

        private void _myMqtt_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
                 // en annen måte å teste koden, fungerer, men ikke slikt vi ønsker. 
            //if (_myMqtt.HelpMessage.Message == null && _myMqtt.HelpMessage.Message == string.Empty) return;

            //var item = new TodoItem
            //{
            //    Text = _myMqtt.HelpMessage.Message,
            //    Complete = false
            //};

            //Todoitemlist.Add(item);
            lock (lockObject)
            {
                using (Context db = new Context("bachlor-ele301.database.windows.net", "Fawad", "123456Rev", "bachlor-ele301"))
                {
                    Todoitemlist.Clear();
                    var items = db.GetData("select * from TodoItem Where complete=0");

                    foreach (var it in items)
                    {
                        Todoitemlist.Add(it);
                    }

                    NotifyPropertyChanged("Todoitemlist");
                }
            }
        }


        //Henter ut data fra Cloud SQL azure. 
        public void sqlazure()
        {
            //Definerer Clienten, henter ut fra nettsiden Azure 
            MobileServiceClient _mobileService = new MobileServiceClient("https://mittsykehusopphold.azurewebsites.net");
            MyMobileClient _myMobileClient = new MyMobileClient(_mobileService);

            //Definerer Databasen , angir authorization type, Bruker, Passord og Server
            using (Context db = new Context("bachlor-ele301.database.windows.net", "Fawad", "123456Rev", "bachlor-ele301"))
            {
                Todoitemlist = db.GetData("select * from TodoItem Where complete=0");

                itemListView.ItemsSource = Todoitemlist;
                NotifyPropertyChanged("Todoitemlist");
            }
            //itemListView.SelectedItem = Todoitemlist.FirstOrDefault();
        }


        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            TodoListChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private static void SubscribeTopic()
        {
            try
            {
                _myMqtt.SubscribeTopic();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public string Topic
        {
            get
            {
                return _topic;
            }
            set
            {
                _topic = value;
            }
        }

        public ObservableCollection<TodoItem> Todoitemlist
        {
            get => _todoitemlist;
            set
            {
                _todoitemlist = value;
                //NotifyPropertyChanged();
            }
        }

               // en funkjson for å lage fra applikasjonen til Sql database. 
        //public async void Onmore(object sender, EventArgs e)
        //{
        //    var mi = ((MenuItem)sender);
        //    var todolist = mi.CommandParameter as TodoItem;

        //    //Context db = new Context("bachlor-ele301.database.windows.net", "Fawad", "123456Rev", "bachlor-ele301");
        //    //db.save("save TodoItemlist set id='" + todolist.id + "'");
        //    db.Save(todolist);
        //    Todoitemlist = db.GetData("select * from Todoitem  Where complete=0");

        //    await DisplayAlert("Lagre", mi.CommandParameter + " Lagrer vasrlet til Database", "OK");  
        //}


        private void Cell_Tapped(object sender, EventArgs e)
        {
            var test = this.FindByName<Cell>("tapped");
            DisplayAlert("Varsel", "legge til varsler her" , "Ok");
        }

        public  void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            var todolist = mi.CommandParameter as TodoItem;

            using (Context db = new Context("bachlor-ele301.database.windows.net", "Fawad", "123456Rev", "bachlor-ele301"))
            {
                db.UpdateData("update TodoItem set complete=1 where id='" + todolist.id + "'");

                var items = db.GetData("select * from TodoItem Where complete=0");

                Todoitemlist.Clear();

                foreach (var it in items)
                {
                    Todoitemlist.Add(it);
                }

                NotifyPropertyChanged("Todoitemlist");
                DisplayAlert("Varsel", mi.CommandParameter + " Sletter varslet", "Ok");
            }
        }
    }
}
